data{
   int<lower=1> M;
   int<lower=1> P;
   matrix[M,P] X;//design matrix
   int<lower=0> N[M];//number of claims
   vector<lower=0>[M] ws;// Durations
 }

parameters{
  vector[P] beta;
}

transformed parameters{
   vector[M] lambda;
   lambda =  exp( X* beta);
}

model{
  
   beta[1] ~ normal(-1.5,1);
   beta[2:num_elements(beta)] ~ normal(0,1);   
   N ~ poisson( ws .* lambda);
}

generated quantities{
  int replicated_samples[M];
  replicated_samples = poisson_rng( ws .* lambda);
}
