// Include functions for the unifed distribution
functions{
  
  real unifed_kappa(real theta){
    
  if( fabs(theta ) < 1e-6)
    return 0;

  if( theta <=50 )
    return log( expm1(theta) / theta  );

  return theta - log (theta);	
}

/**
 * Applies unifed_kappa to each element of a vector.
 * 
 * @param theta A vector.
 *
 * @return A vector whose elements are unifed_kappa applied to each
 * element of theta.
 */
vector unifed_kappa_v(vector theta){
  int N=num_elements(theta);
  vector[N] result;
  
  for(i in 1:N)
    result[i]=unifed_kappa(theta[i]);

  return result;
}

real unifed_kappa_prime(real theta){
  real tol=1e-7;
  real return_value;
  if (fabs(theta) <= machine_precision()){
    return_value = 0.5;
  } else{
    return_value = -1/expm1(-theta) - 1/theta ;
    }
return return_value;
}

real unifed_kappa_double_prime(real theta){
  real tol=1e-7;
  real return_value;
  if (fabs(theta) <= tol){
    return_value = 1./12.;
  }else{
    if(theta > -100)
      return_value = pow(1/theta,2)  - exp(-theta)/pow(expm1(-theta),2);
    else
      return_value = pow(1/theta,2);
  }
  return return_value;
}
  
real unifed_lpdf(real x,real theta){
  if( x < 0 || x > 1  )
    reject("x must be between 0 and 1");
  return x * theta - unifed_kappa(theta);
}

real unifed_quantile(real p,real theta){
  if( p<0 || p>1)
    reject("p must be between 0 and 1");
  if( fabs(theta) < 1e-6)
    return p;
  else
    return log(p * ( exp(theta) -1 ) + 1 ) / theta;
}

real unifed_rng(real theta){
  return unifed_quantile(uniform_rng(0,1),theta);
}

real unifed_lcdf(real x,real theta){
  
  real tol = 1e-7;
  
  if( x <= 0)
    return negative_infinity();
  if( x >= 1 )
    return 0;

  if( fabs(theta) < tol )
    return log(x);

  if( theta <=50 )
    return log( expm1(x*theta) / expm1(theta)  );
  
  return  (x-1) * theta + log1m_exp( - theta * x) - log1m_exp( - theta); 
  
}



real unifed_kappa_prime_inverse(real mu){
  
  real tol=1e-7;
  int maxit=10000000;
  real found_solution = 0;
  real y_prime;
  int iter=1;

  real old_x = 0;
  real y;
  real new_x;


  if ( fabs(mu - 0.5) <= 1e-5 )
    return 0;

  else{
    
    while(iter <= maxit){
      y = unifed_kappa_prime(old_x) - mu;
      y_prime = unifed_kappa_double_prime(old_x);
      new_x = old_x - y/y_prime;

      if ( fabs(new_x - old_x) <= tol * fabs(new_x)  ){
	found_solution = 1;
	break;
      }
      old_x = new_x;
      iter+=1;
    }

    if (found_solution){
	return new_x;
    }
    else
      return  not_a_number();
  }
}

real unifed_unit_deviance(real y,real mu){
  real y_inv=unifed_kappa_prime_inverse(y);
  real mu_inv=unifed_kappa_prime_inverse(mu);

  return 2 * ( y*( y_inv - mu_inv ) - unifed_kappa(y_inv) + unifed_kappa(mu_inv) );
  
}

/**
 * Applies unifed_kappa to each element of a vector.
 * 
 * @param theta A vector.
 *
 * @return A vector whose elements are unifed_kappa applied to each
 * element of theta.
 */
vector unifed_kappa_prime_inverse_v(vector mu){
  int N=num_elements(mu);
  vector[N] result;
  for(i in 1:N){
    result[i] = unifed_kappa_prime_inverse(mu[i]);
  }
  return result;
}

/**
 * Increases the log probability accumulator with the likelihood
 * function of a unifed GLM.
 *
 * @param y vector of observed responses
 * 
 * @param theta vector containing the cenonical parameters of each
 * class of the glm.
 *
 * @param weights vector with the number of observations in each class.
 *
 */
void unifed_glm_lp(vector y, vector theta, vector weights){
  target += dot_product(weights,(y .* theta  - unifed_kappa_v(theta)));
}

}


data{
  int<lower=1> N; // Number of observations
  vector<lower=0,upper=1>[N] durations; // Vector of durations
  int Nclaims[N]; // Number of claims for each risk
  int Nlosses; // Total number of losses
  vector<lower=0>[Nlosses] loss; // The amount of each loss
}

transformed data{
  vector<lower=0>[Nlosses] scaled_loss;
  scaled_loss = loss / 500;
}

parameters{
  real<lower=0> lambda;
  real<lower=0> sev_scaled_mu;  // mean of the gamma
  real<lower=0> sev_scaled_var;  // variance of the gamma
  real<lower=0,upper=1> unifed_mu; // mean of the unifed
}

transformed parameters{
  real<lower=0> sev_mu;
  real<lower=0> sev_var;  
  real<lower=0> alpha;
  real<lower=0> beta;
  real unifed_theta;
  
  sev_mu = sev_scaled_mu * 5e2;
  sev_var = sev_scaled_var * 25e4;
  alpha = sev_scaled_mu^2 / sev_scaled_var;
  beta = sev_scaled_mu / sev_scaled_var;
  
  unifed_theta = unifed_kappa_prime_inverse(unifed_mu);
}

model{
  // priors for the parameters
  lambda ~ normal(0,10) T[0,];
  sev_scaled_mu ~ normal(0,1)T[0,];
  sev_scaled_var ~ normal(0,1)T[0,];
  unifed_mu ~ normal(0.5,10)T[0,1];
  //Distributions
  Nclaims ~ poisson(lambda * durations);
  scaled_loss ~ gamma(alpha,beta);
  for(i in 1:N)
    durations[i] ~ unifed(unifed_theta);
  
}

generated quantities{

  // frequency definitions
  int freq_replicated_samples[N];
  real freq_replicated_weighted_mean;
  real freq_replicated_variance;
  real freq_replicated_proportion_of_zeros=0;

  // severity definitions
  vector[Nlosses] sev_replicated_samples;
  real sev_replicated_mean;
  real sev_replicated_min; // The minimum loss
  real sev_replicated_max; // The max loss
  real sev_replicated_sd; // The standar deviation of the losses

  // unifed definitions
  vector<lower=0,upper=1>[N] exposure_replicated_samples;
  real exposure_replicated_min;
  real exposure_replicated_max;
  real exposure_replicated_mean;
  real exposure_replicated_sd;

  // total loss 
  real replicated_total_loss=0;
  // total with unknown number of insureds for next year
  real replicated_total_loss_1=0;
  int Ninsureds_1; // Necessary for premium computations
  real average_total_loss_1;
  
  // total loss with unknown number of insured and unknown durations
  real replicated_total_loss_2=0;
  real average_total_loss_2;

  

  // frequency generated quantities
  freq_replicated_samples = poisson_rng(lambda * durations);  
  freq_replicated_weighted_mean = sum(freq_replicated_samples) / sum(durations);
  freq_replicated_variance = dot_self(to_vector(freq_replicated_samples) - durations*lambda )/N ;
  for(i in 1:N){
    if( freq_replicated_samples[i] == 0 )
      freq_replicated_proportion_of_zeros+=1;
  }  
  freq_replicated_proportion_of_zeros/=N;

  // severity generated quantities
  for( i in 1:Nlosses)
    sev_replicated_samples[i] = gamma_rng(alpha,beta) * 500;
  
  sev_replicated_min = min(sev_replicated_samples);
  sev_replicated_max = max(sev_replicated_samples);
  sev_replicated_mean = mean(sev_replicated_samples);
  sev_replicated_sd = sd(sev_replicated_samples);

  for(i in 1:N){
    int Naccidents;
    exposure_replicated_samples[i] = unifed_rng(unifed_theta);
    Naccidents = poisson_rng(lambda);
    
    for(j in 1:Naccidents){
      replicated_total_loss += gamma_rng(alpha,beta) * 500;
    }
    
  }

  exposure_replicated_mean=mean(exposure_replicated_samples);
  exposure_replicated_sd=sd(exposure_replicated_samples);
  exposure_replicated_min=min(exposure_replicated_samples);
  exposure_replicated_max=max(exposure_replicated_samples);

  // replicated total loss with unknown number of insureds
  {
    int Naccidents;
    // considering exposure as well
    int Naccidents_exp;
    real total_exposure=0;
    // mean 30 and variance 39(= 30 + 30^2/100)
    Ninsureds_1 = neg_binomial_2_rng(30,100);    
    // total loss with exposure=1
    Naccidents = poisson_rng(lambda * Ninsureds_1 );
    for(j in 1:Naccidents)
      replicated_total_loss_1 += gamma_rng(alpha,beta) * 500 ;

    average_total_loss_1 = replicated_total_loss_1 / Ninsureds_1;
    
    // total loss including exposure
    for(i in 1:Ninsureds_1)
      total_exposure+= unifed_rng(unifed_theta);
    
    Naccidents_exp = poisson_rng(lambda * total_exposure);

    for(j in 1:Naccidents_exp)
      replicated_total_loss_2+= gamma_rng(alpha,beta) * 500 ;

    average_total_loss_2 = replicated_total_loss_2 / total_exposure;
    
  }
    
  
  
}
