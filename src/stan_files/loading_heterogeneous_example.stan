functions{
  
  real unifed_kappa(real theta){
    
  if( fabs(theta ) < 1e-6)
    return 0;

  if( theta <=50 )
    return log( expm1(theta) / theta  );

  return theta - log (theta);	
}

/**
 * Applies unifed_kappa to each element of a vector.
 * 
 * @param theta A vector.
 *
 * @return A vector whose elements are unifed_kappa applied to each
 * element of theta.
 */
vector unifed_kappa_v(vector theta){
  int N=num_elements(theta);
  vector[N] result;
  
  for(i in 1:N)
    result[i]=unifed_kappa(theta[i]);

  return result;
}

real unifed_kappa_prime(real theta){
  real tol=1e-7;
  real return_value;
  if (fabs(theta) <= machine_precision()){
    return_value = 0.5;
  } else{
    return_value = -1/expm1(-theta) - 1/theta ;
    }
return return_value;
}

real unifed_kappa_double_prime(real theta){
  real tol=1e-7;
  real return_value;
  if (fabs(theta) <= tol){
    return_value = 1./12.;
  }else{
    if(theta > -100)
      return_value = pow(1/theta,2)  - exp(-theta)/pow(expm1(-theta),2);
    else
      return_value = pow(1/theta,2);
  }
  return return_value;
}
  
real unifed_lpdf(real x,real theta){
  if( x < 0 || x > 1  )
    reject("x must be between 0 and 1");
  return x * theta - unifed_kappa(theta);
}

real unifed_quantile(real p,real theta){
  if( p<0 || p>1)
    reject("p must be between 0 and 1");
  if( fabs(theta) < 1e-6)
    return p;
  else
    return log(p * ( exp(theta) -1 ) + 1 ) / theta;
}

real unifed_rng(real theta){
  return unifed_quantile(uniform_rng(0,1),theta);
}

real unifed_lcdf(real x,real theta){
  
  real tol = 1e-7;
  
  if( x <= 0)
    return negative_infinity();
  if( x >= 1 )
    return 0;

  if( fabs(theta) < tol )
    return log(x);

  if( theta <=50 )
    return log( expm1(x*theta) / expm1(theta)  );
  
  return  (x-1) * theta + log1m_exp( - theta * x) - log1m_exp( - theta); 
  
}



real unifed_kappa_prime_inverse(real mu){
  
  real tol=1e-7;
  int maxit=10000000;
  real found_solution = 0;
  real y_prime;
  int iter=1;

  real old_x = 0;
  real y;
  real new_x;


  if ( fabs(mu - 0.5) <= 1e-5 )
    return 0;

  else{
    
    while(iter <= maxit){
      y = unifed_kappa_prime(old_x) - mu;
      y_prime = unifed_kappa_double_prime(old_x);
      new_x = old_x - y/y_prime;

      if ( fabs(new_x - old_x) <= tol * fabs(new_x)  ){
	found_solution = 1;
	break;
      }
      old_x = new_x;
      iter+=1;
    }

    if (found_solution){
	return new_x;
    }
    else
      return  not_a_number();
  }
}

real unifed_unit_deviance(real y,real mu){
  real y_inv=unifed_kappa_prime_inverse(y);
  real mu_inv=unifed_kappa_prime_inverse(mu);

  return 2 * ( y*( y_inv - mu_inv ) - unifed_kappa(y_inv) + unifed_kappa(mu_inv) );
  
}

/**
 * Applies unifed_kappa to each element of a vector.
 * 
 * @param theta A vector.
 *
 * @return A vector whose elements are unifed_kappa applied to each
 * element of theta.
 */
vector unifed_kappa_prime_inverse_v(vector mu){
  int N=num_elements(mu);
  vector[N] result;
  for(i in 1:N){
    result[i] = unifed_kappa_prime_inverse(mu[i]);
  }
  return result;
}

/**
 * Increases the log probability accumulator with the likelihood
 * function of a unifed GLM.
 *
 * @param y vector of observed responses
 * 
 * @param theta vector containing the cenonical parameters of each
 * class of the glm.
 *
 * @param weights vector with the number of observations in each class.
 *
 */
void unifed_glm_lp(vector y, vector theta, vector weights){
  target += dot_product(weights,(y .* theta  - unifed_kappa_v(theta)));
}

}

data{
  // input for frequency model
  int<lower=1> M_freq;
  int<lower=1> P_freq;
  matrix[M_freq,P_freq] X_freq;//design matrix
  int<lower=0> N_freq[M_freq];//number of claims
  vector<lower=0>[M_freq] ws_freq;// Durations

  // inputs for severity model
  int<lower=1> M_sev; // Number of rows of the design matrix
  int<lower=1> P_sev; // Number of columns of the design matrix
  matrix[M_sev,P_sev] X_sev; // Design matrix
  vector<lower=0>[M_sev] y_sev; // response variable
  vector<lower=0>[M_sev] ws_sev; // Number of claims

  // inputs for duration model
  int<lower=1> M_dur; //Rows in the design matrix
  int<lower=1> P_dur; //Columns in the design matrix
  matrix[M_dur,P_dur] X_dur;
  vector<lower=0,upper=1>[M_dur] y_dur;
  int<lower=1> ws_dur[M_dur]; //Number of observations in each class

  // Prediction data
  int<lower=1> M_pred_freq;
  int<lower=1> P_pred_freq;
  int<lower=1> M_pred_sev;
  int<lower=1> P_pred_sev;
  int<lower=1> M_pred_dur;
  int<lower=1> P_pred_dur;
  matrix[M_pred_freq,P_pred_freq] X_pred_freq;
  matrix[M_pred_sev,P_pred_sev] X_pred_sev;
  matrix[M_pred_dur,P_pred_dur] X_pred_dur;
  int<lower=1> Nrisks_pred[M_pred_freq];
}

parameters{
  vector[P_freq] beta_freq;
  vector[P_sev] beta_sev;
  real<lower=0> phi_sev;
  vector[P_dur] beta_dur;
  
}

transformed parameters{
  vector[M_sev] mu_sev;
  vector[M_freq] lambda_freq;
  vector[M_dur] theta_dur;
  lambda_freq =  exp( X_freq * beta_freq);
  mu_sev = exp(X_sev * beta_sev);
  theta_dur = X_dur * beta_dur;
}

model{
  
  beta_freq[1] ~ normal(-1.5,1);
  beta_freq[2:num_elements(beta_freq)] ~ normal(0,1);   
  N_freq ~ poisson( ws_freq .* lambda_freq);
  
  beta_sev[1] ~ normal(8,3);
  beta_sev[2:num_elements(beta_sev)] ~ normal(0,2);   
  phi_sev ~ normal(10,20) T[0,];
  y_sev ~ gamma( ws_sev/phi_sev , ws_sev ./ (phi_sev*mu_sev) );

  beta_dur ~ normal(0,20);  
  unifed_glm_lp(y_dur, theta_dur , to_vector(ws_dur));
  
}


generated quantities{
  // Total loss of the portfolio:
  real total_loss=0;
  // Total duration of the portfolio
  real total_duration=0;
  // Average loss of the portfolio
  // Unearned premium is returned
  real average_total_loss;
  // Unearned premium is not returned
  real average_total_loss_2;
  // Total loss of each class:
  vector[M_pred_sev] class_loss = rep_vector(0,M_pred_sev);
  // Total duration of each class
  vector[M_pred_sev] class_duration = rep_vector(0,M_pred_sev);
  // Average loss of each class
  vector[M_pred_sev] class_average_loss = rep_vector(0,M_pred_sev);
  // Means of each particular model
  vector[M_pred_sev] frequency_mean;
  vector[M_pred_sev] duration_theta;
  vector[M_pred_sev] severity_mean;

  int Nrisks[M_pred_sev];

  frequency_mean = exp( X_pred_freq * beta_freq);
  duration_theta = X_pred_dur * beta_dur;
  severity_mean = exp(X_pred_sev * beta_sev);
  
  for(i in 1:M_pred_sev){
    int Naccidents;
    //Number of clients for next year can increase between 0 and 20%
    // int Nrisks = round( Nrisks_pred[i] * ( 1 + uniform_rng(0,0.2) ) );

    // The dispersion is chosen so the standard deviation is mu/2
    Nrisks[i] = neg_binomial_2_rng(Nrisks_pred[i] , (Nrisks_pred[i])/ 10.0  );
    
    /* if(Nrisks_pred[i] > 1) */
    /*   Nrisks = neg_binomial_2_rng(Nrisks_pred[i] , Nrisks_pred[i]/ (Nrisks_pred[i] -1.0)  ); */
    /* else */
    /*   Nrisks = neg_binomial_2_rng(Nrisks_pred[i] , Nrisks_pred[i] ); */

    
    //Get the total duration of the class
    for(j in 1:(Nrisks[i])){
      class_duration[i]+= unifed_rng( duration_theta[i] );
    }

    if( class_duration[i] >0 ){

      Naccidents = poisson_rng(class_duration[i] * frequency_mean[i]);

      if(Naccidents > 0){
	class_loss[i] = gamma_rng( pow(Naccidents,2) / phi_sev , Naccidents / ( phi_sev * severity_mean[i]  ) );
      }

      class_average_loss[i] = class_loss[i] / class_duration[i];
    }}

  total_loss = sum(class_loss);
  total_duration = sum(class_duration);
  average_total_loss = total_loss / total_duration;
  average_total_loss_2 = total_loss / sum(Nrisks);
}
