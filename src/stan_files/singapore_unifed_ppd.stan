functions{
  real unifed_kappa(real theta){
    
  if( fabs(theta ) < 1e-6)
    return 0;

  if( theta <=50 )
    return log( expm1(theta) / theta  );

  return theta - log (theta);	
}

real unifed_kappa_prime(real theta){
  real tol=1e-7;
  real return_value;
  if (fabs(theta) <= machine_precision()){
    return_value = 0.5;
  } else{
    return_value = -1/expm1(-theta) - 1/theta ;
    }
return return_value;
}

real unifed_kappa_double_prime(real theta){
  real tol=1e-7;
  real return_value;
  if (fabs(theta) <= tol){
    return_value = 1./12.;
  }else{
    if(theta > -100)
      return_value = pow(1/theta,2)  - exp(-theta)/pow(expm1(-theta),2);
    else
      return_value = pow(1/theta,2);
  }
  return return_value;
}
  
real unifed_lpdf(real x,real theta){
  if( x < 0 || x > 1  )
    reject("x must be between 0 and 1");
  return x * theta - unifed_kappa(theta);
}

real unifed_quantile(real p,real theta){
  if( p<0 || p>1)
    reject("p must be between 0 and 1");
  if( fabs(theta) < 1e-6)
    return p;
  else
    return log(p * ( exp(theta) -1 ) + 1 ) / theta;
}

real unifed_rng(real theta){
  return unifed_quantile(uniform_rng(0,1),theta);
}

real unifed_lcdf(real x,real theta){
  
  real tol = 1e-7;
  
  if( x <= 0)
    return negative_infinity();
  if( x >= 1 )
    return 0;

  if( fabs(theta) < tol )
    return log(x);

  if( theta <=50 )
    return log( expm1(x*theta) / expm1(theta)  );
  
  return  (x-1) * theta + log1m_exp( - theta * x) - log1m_exp( - theta); 
  
}



/* real kappa_prime_inverse(real mu){ */
  
/*   real tol=1e-7; */
/*   int maxit=10000000; */
/*   real found_solution = 0; */
/*   real y_prime; */
/*   int iter=1; */
/*   real small_number=0; */
/*   real old_x = 0; */
/*   real y; */
/*   real new_x; */
/*   real mu_aux; */

/*   if( mu < 0.1){ */
/*     mu_aux = 1 - mu; */
/*     small_number=1; */
/*   }else{ */
/*     mu_aux = mu; */
/*   } */

/*   if ( fabs(mu - 0.5) <= 1e-5 ) */
/*     return 0; */

/*   else{ */
    
/*     while(iter <= maxit){ */
/*       y = unifed_kappa_prime(old_x) - mu_aux; */
/*       y_prime = unifed_kappa_double_prime(old_x); */
/*       new_x = old_x - y/y_prime; */

/*       if ( fabs(new_x - old_x) <= tol * fabs(new_x)  ){ */
/* 	found_solution = 1; */
/* 	break; */
/*       } */
/*       old_x = new_x; */
/*       iter+=1; */
/*     } */

/*     if (found_solution){ */
/*       if (small_number) */
/* 	return -new_x; */
/*       else */
/* 	return new_x; */
/*     } */
/*     else */
/*       return  not_a_number(); */
/*   } */
/* } */


real unifed_kappa_prime_inverse(real mu){
  
  real tol=1e-7;
  int maxit=10000000;
  real found_solution = 0;
  real y_prime;
  int iter=1;

  real old_x = 0;
  real y;
  real new_x;


  if ( fabs(mu - 0.5) <= 1e-5 )
    return 0;

  else{
    
    while(iter <= maxit){
      y = unifed_kappa_prime(old_x) - mu;
      y_prime = unifed_kappa_double_prime(old_x);
      new_x = old_x - y/y_prime;

      if ( fabs(new_x - old_x) <= tol * fabs(new_x)  ){
	found_solution = 1;
	break;
      }
      old_x = new_x;
      iter+=1;
    }

    if (found_solution){
	return new_x;
    }
    else
      return  not_a_number();
  }
}

real unifed_unit_deviance(real y,real mu){
  real y_inv=unifed_kappa_prime_inverse(y);
  real mu_inv=unifed_kappa_prime_inverse(mu);

  return 2 * ( y*( y_inv - mu_inv ) - unifed_kappa(y_inv) + unifed_kappa(mu_inv) );
  
}

}

data{
  int<lower=1> N;
  vector<lower=0,upper=1>[N] y;
}

transformed data{
  real y_mean=mean(y);
}

parameters{
  real theta;
}

model{
  theta ~ normal(0,20);
  target+= N* (y_mean * theta -  unifed_kappa(theta));
}

generated quantities{
  real rep_y;
  rep_y = unifed_rng(theta);
}
