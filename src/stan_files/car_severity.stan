data {
  int<lower=1> M; // Number of rows of the design matrix
  int<lower=1> P; // Number of columns of the design matrix
  matrix[M,P] X; // Design matrix
  vector<lower=0>[M] y; // response variable
  vector<lower=0>[M] ws; // Number of claims
}

parameters {
  vector[P] beta;
  real<lower=0> phi;
}

transformed parameters{
  vector[M] mu;
  mu = exp(X * beta);
}

model{  
  beta[1] ~ normal(8,3);
  beta[2:num_elements(beta)] ~ normal(0,2);   
  phi ~ normal(10,20) T[0,];
  y ~ gamma( ws/phi , ws ./ (phi*mu) );
}

generated quantities{
  vector[M] replicated_samples;
  for(i in 1:M){
    replicated_samples[i] = gamma_rng( ws[i]/phi, ws[i]/(phi*mu[i] ));
  }
}
