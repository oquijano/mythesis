data {
  int<lower=1> N;
  vector<lower=0,upper=1>[N] y;
}

parameters{
  real<lower=0> a;
  real<lower=0> b;
}

model{
  a ~ normal(4,20) T[0,];
  b ~ normal(4,20) T[0,];
  y ~ beta(a,b);
}

generated quantities{
  real rep_y;
  rep_y = beta_rng(a,b);
}
