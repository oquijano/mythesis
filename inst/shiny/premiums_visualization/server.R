library(magrittr)
library(unifed)


server <- function(input,output){

    qs <- seq(0,1,by=0.01)
    
    premiums <- reactiveValues()

    observe({

        unifed.theta <- unifed.kappa.prime.inverse(input$duration_mean)
        gamma.shape <- input$gamma_mean^2 / input$gamma_sd^2
        gamma.scale <- input$gamma_sd^2 / input$gamma_mean

        premiums$annual.pure <- input$poisson_mean * input$gamma_mean
        premiums$pure <- premiums$annual.pure * input$duration_mean

        Nclients <- rnbinom(input$Nsims,
                            mu=input$nb_mean,
                            size=input$nb_mean^2/(input$nb_sd^2 - input$nb_mean))

        average.losses <- vapply(Nclients,
                                 function(n){
                                     if(n>0){
                                         duration <- runifed(n,unifed.theta)
                                         lambda <-  duration * input$poisson_mean
                                         nclaims <- rpois(1,sum(lambda))
                                         if(nclaims > 0){
                                             total.loss <- rgamma(1,shape=nclaims*gamma.shape,scale=gamma.scale)
                                             c(total.loss / sum(duration) , total.loss / n )
                                             }
                                         else
                                             c(0,0)
                                         ## Entry one: Average loss per duration
                                         ## Entry two: Average loss per client
                                         
                                     } else{
                                         c(0,0)
                                     }},
                                 FUN.VALUE=c(1,1))


        av.losses.r <- average.losses[1,]
        av.losses.nr <- average.losses[2,]
        
        premiums$cqr <- quantile(av.losses.r,probs=qs,na.rm=T)
        premiums$cqnr <- quantile(av.losses.nr,probs=qs,na.rm=T)


        premiums$ctr <- sapply(qs,
                               function(x){
                                   the.quantile <- quantile(av.losses.r,x,na.rm=T)
                                        #denominator <- mean(total.loss > the.quantile)
                                   denominator <- 1-x
                                   the.premium <- mean(av.losses.r  * (av.losses.r > the.quantile) ) / denominator
                                   if(is.nan(the.premium))
                                       max(av.losses.r)
                                   else
                                       the.premium
                               })

        premiums$ctnr <- sapply(qs,
                               function(x){
                                   the.quantile <- quantile(av.losses.nr,x,na.rm=T)
                                        #denominator <- mean(total.loss > the.quantile)
                                   denominator <- 1-x
                                   the.premium <- mean(av.losses.nr  * (av.losses.nr > the.quantile) ) / denominator
                                   if(is.nan(the.premium))
                                       max(av.losses.nr)
                                   else
                                       the.premium
                               })
        
    })
    
    

    output$premiums_plot_r <- renderPlot({                                        

        plot(qs,premiums$cqr,type="l",ylim=c(0,input$r_max_y),xlab="Level",ylab="Premium",main="Premiums that return the unearned premium")
        points(qs,premiums$ctr,type="l",lty=2)
        abline(h=premiums$annual.pure,lty=3)
        
        legend("topleft",
               legend=c("Quantile Premium","Tail Quantile Premium","Annual Pure Premium"),
               lty=1:3)
    })

    output$premiums_plot_nr <- renderPlot({                                        

        plot(qs,premiums$cqnr,type="l",ylim=c(0,input$nr_max_y),xlab="Level",ylab="Premium",main="Premiums that do not return the unearned premium")
        points(qs,premiums$ctnr,type="l",lty=2)
        abline(h=premiums$pure,lty=3)
        
        legend("topleft",
               legend=c("Quantile Premium","Tail Quantile Premium","Pure Premium"),
               lty=1:3)
    })
    
    
}



