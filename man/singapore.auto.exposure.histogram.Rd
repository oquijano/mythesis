% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/singapore.auto.exposure.histogram.R
\name{singapore.auto.exposure.histogram}
\alias{singapore.auto.exposure.histogram}
\title{Histogram and unifed fit for SingaporeAuto exposure}
\usage{
singapore.auto.exposure.histogram()
}
\description{
Computes the mle of the unifed parameter for the exposure in the
SingaporeAuto dataset from the insuranceData package. Then it plots
the histogram of these observations along with the density of the
fitted unifed.
}
