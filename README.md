This repository contains an R package for reproducing the examples
presented in my PhD thesis, which has results about Bayesian GLMs and
their use for insurance premium estimation.

In order to install the package you can use the
[devtools](https://www.rstudio.com/products/rpackages/devtools/)
package:

```R
devtools::install_gitlab("oquijano/mythesis")
```

On windows it is necessary to have
[Rtools](https://cran.r-project.org/bin/windows/Rtools/) installed for
installing and using this package.

The thesis source files imports several tex and pdf files generated
with this package. The function `write.thesis.files` copies all such
files into a folder.

The following sections show how to obtain the tables and figures from
the last three chapters. Besides installing the package it is
necessary to load it with
```R
library(mythesis)
```


## Chapter 7: Entropic Credibility

It is common in Bayesian point estimation to use the euclidean
distance as a loss function. The problem is that the estimators
obtained are not consistent under one-on-one transformations. We
propose the use of the relative entropy as a loss function, which
gives consistent point estimators under one-on-one transformations. A
theorem is given that outlines an algorithm for finding such estimator
for GLMs. The chapter contains two examples, one for a frequency model
and another one for a severity model.

### Frequency Model Example

It is first necessary to obtain the Hamiltonian Monte Carlo (HMC)
simulations for the model. For this simply run
```R
fit.car.frequency()
```
This saves the simulations and the obtained coefficients in an
environment called `car.env` contained in the package. The
coefficients table (Table 7.5) was obtained with
```R
coeff.summary(car.env$freq.entropic.betas,
              rstan::extract(car.env$freq.stan.glm,"beta")$beta)
```
The residuals plot (Figure 7.1) and the number of zero residuals plot
(Figure 7.3) were obtained respectively with
```R
plot.car.frequency.residuals()
## The following takes some time to run
plot.car.frequency.zero.residuals()
```
The comparison between the observed and replicated residuals (Figure
7.2) was obtained with
```R
plot.car.frequency.replicated.residuals()
```

### Severity Model Example
For obtaining the HMC simulations
```R
fit.car.severity()
```
Coefficients table (Table 7.7)
```R
coeff.summary(car.env$sev.entropic.betas,
              rstan::extract(car.env$sev.stan.glm,"beta")$beta)
```
Residuals plot (Figure 7.4)
```R
par(mfrow=c(1,2))
plot.car.severity.residuals()
plot.car.severity.normalized.residuals()
```

## Chapter 8: The Unifed Distribution

The Unifed is a distribution with support on (0,1). It is an
exponential dispersion family and it can be used as the response
distribution of a GLM. The chapter contains two examples. A
frequentist unifed GLM and Bayesian unifed GLM. The following code
fits both model

For obtaining the HMC simulations
```R
fit.unifed.models()
```

### Frequentist GLM Example

Coefficients table (Table 8.2)
```R
summary(unifed.env$glm)
```

Residuals plot (Figure 8.6)
```R
plot.unifed.glm.residuals()
```

### Bayesian GLM Example

Coefficients table (Table 8.4)
```R
coeff.summary(unifed.env$entropic.betas,
              rstan::extract(unifed.env$stan.glm,"beta")$beta)
```

Residuals plot (Figure 8.7)
```R
plot.unifed.stan.glm.residuals()
```

## Chapter 9: Risk Loadings for GLMs
The well known risk measures VaR and TVaR are used to determine the
premium risk loading of a portfolio. The idea allows to consider the
number of risks in the portfolio and their duration as random variables.

The Chapter has a homogeneous and a heterogeneous example. The
following two lines obtain the HMC simulations
```R
fit.loadings.homogeneous.example()
fit.loadings.heterogeneous.example()
```

### Homogeneous Example

The values in the mock homogeneous dataset (Table 9.1) can be saved in
a variable `homogeneous.example.data` with the following statement.
```R
homogeneous.example.data <- loading.homogeneous.mock.dataset()
```

Test quantities plot for the frequency model (Figure 9.1)
```R
plot.loading.homogeneous.frequency.graph()
```

Test quantities plot for the severity model (Figure 9.2)
```R
plot.loading.homogeneous.severity.graph()
```

Test quantities plot or the exposure model (Figure 9.3)
```R
plot.loading.homogeneous.exposure.graph()
```

The function `write.loadings.premiums.table` writes a file with the
tex code for the premiums table (Table 9.2).

### Heterogeneous Example

The plots a and b in Figure 9.4 can be obtained with the following
functions
```R
plot.loading.heterogeneous.cqr()
plot.loading.heterogeneous.ctr()
```

The computations for getting the values of $\beta^*$ Table 9.3, are
performed with the following line of code
```R
compute.loadings.heterogeneous.example(duration.breaks=c(1,2,5),chains=chains)
```
The plots of Figure 9.5 can be obtained with the following functions
```R
plot.loading.heterogeneous.categories.cqr()
plot.loading.heterogeneous.categories.ctr()
plot.loading.heterogeneous.categories.cqnr()
plot.loading.heterogeneous.categories.ctnr()
```
Figure 9.6 can be obtained with
```R
plot.loading.heterogeneous.categories.ctr.expected.mean()
```



<!-- LocalWords: Entropic entropic unifed frequentist durations -->
