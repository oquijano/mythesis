.onLoad <- function(libname, pkgname) {
  if (length(stanmodels) != 0) {
    modules <- paste0("stan_fit4", names(stanmodels), "_mod")
    for (m in modules) loadModule(m, what = TRUE)
  } else {
    message("No stan programs to compile were found.")
  }
  options(mc.cores = parallel::detectCores())
  car.env <- new.env()
  assign("car.env",car.env,globalenv() )
  unifed.env <- new.env()
  assign("unifed.env",unifed.env,globalenv() )
  loadings.env <- new.env()
  assign("loadings.env",loadings.env,globalenv())
}
